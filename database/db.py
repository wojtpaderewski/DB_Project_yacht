from dotenv import dotenv_values
from flask_sqlalchemy import SQLAlchemy

config = dotenv_values(".env")

db_name = config.get('DB_NAME')
db_password = config.get('DB_PASSWORD')
db_user = config.get('DB_USER')
db_host = config.get('DB_HOST')

def init_db(app):
    uri = 'postgresql://' + db_user + ':' + db_password + '@' + db_host + '/' + db_name
    app.config['SQLALCHEMY_DATABASE_URI'] = uri
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db = SQLAlchemy(app)
    return db

    