from flask import Flask, render_template, request
from database.db import init_db

app = Flask(__name__)
db = init_db(app)       

@app.route('/')

def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.debug = True
    app.run()
    